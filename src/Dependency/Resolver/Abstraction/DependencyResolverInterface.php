<?php

namespace WPDesk\DiContainer\Dependency\Resolver\Abstraction;

/**
 * Interface DependencyResolverInterface, abstraction layer for dependency resolver implementation.
 * @package WPDesk\DiContainer
 */
interface DependencyResolverInterface {

	/**
	 * @param string $class_name
	 * @param array $arguments
	 *
	 * @return object
	 */
	public function resolve( string $class_name, array $arguments );

}
