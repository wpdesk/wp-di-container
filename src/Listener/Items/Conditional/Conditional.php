<?php

namespace WPDesk\DiContainer\Listener\Items\Conditional;

/**
 * Interface Conditional, check if service should run next listener for exmaple Initable, Registrable etc.
 * @package WPDesk\DiContainer\Listener\Conditional
 */
interface Conditional {

	public function isActive(): bool;

}
