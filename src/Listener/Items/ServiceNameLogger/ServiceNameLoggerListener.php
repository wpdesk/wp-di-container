<?php

namespace WPDesk\DiContainer\Listener\Items\ServiceNameLogger;

use WPDesk\DiContainer\Listener\Abstraction\AbstractServiceListener;
use WPDesk\DiContainer\Container\Abstraction\ServiceContainerInterface;

/**
 * Class ServiceNameLoggerListener, log servies class names.
 * @package WPDesk\DiContainer\Listener\ServiceNameLogger
 */
final class ServiceNameLoggerListener extends AbstractServiceListener {

	/**
	 * @see AbstractServiceListener::update()
	 */
	public function update( $service, ServiceContainerInterface $service_container ) {
		error_log( get_class( $service ) );
	}

}
