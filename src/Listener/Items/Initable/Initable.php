<?php

namespace WPDesk\DiContainer\Listener\Items\Initable;

/**
 * Interface Initable, add init method to service.
 * @package WPDesk\DiContainer\Listener\Initable
 */
interface Initable {

	/**
	 * @return void
	 */
	public function init();

}
