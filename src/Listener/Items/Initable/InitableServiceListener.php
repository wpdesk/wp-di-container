<?php

namespace WPDesk\DiContainer\Listener\Items\Initable;

use WPDesk\DiContainer\Listener\Abstraction\AbstractServiceListener;
use WPDesk\DiContainer\Container\Abstraction\ServiceContainerInterface;

/**
 * Class InitableServiceListener, chceck if service implements initable interface and run it.
 * @package WPDesk\DiContainer\Listener\Initable
 */
final class InitableServiceListener extends AbstractServiceListener{

	/**
	 * @see AbstractServiceListener::update()
	 */
	public function update( $service, ServiceContainerInterface $service_container ) {
		if ( $service instanceof Initable ) {
			$service->init();
		}
	}
}
