<?php

namespace WPDesk\DiContainer\Listener\Items\Registrable;

use WPDesk\DiContainer\Listener\Abstraction\AbstractServiceListener;
use WPDesk\DiContainer\Container\Abstraction\ServiceContainerInterface;

/**
 * Class RegistrableServiceListener, register class or object as services.
 * @package WPDesk\DiContainer\Listener\Registrable
 */
final class RegistrableServiceListener extends AbstractServiceListener{

	/**
	 * @see AbstractServiceListener::update()
	 */
	public function update( $service, ServiceContainerInterface $service_container ) {
		if(  $service instanceof Registrable ) {
			$servicesToRegister = $service->register();

			if ( !empty( $servicesToRegister ) ) {
				$service_container->register_from_array( $servicesToRegister );
			}
		}
	}
}
