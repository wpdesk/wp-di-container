<?php

namespace WPDesk\DiContainer\Listener\Items\Registrable;

/**
 * Interface Registrable, return array with class names that should be registered to service container.
 * @package WPDesk\DiContainer\Listener\Registrable
 */
interface Registrable {

	/**
	 * @return string[]
	 */
	public function register(): array;

}
