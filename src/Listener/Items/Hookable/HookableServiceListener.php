<?php

namespace WPDesk\DiContainer\Listener\Items\Hookable;

use WPDesk\DiContainer\Listener\Abstraction\AbstractServiceListener;
use WPDesk\DiContainer\Container\Abstraction\ServiceContainerInterface;

/**
 * Class HookableServiceListener, checks if service implements hookable interface and run it.
 * @package WPDesk\DiContainer\Listener\Hookable
 */
final class HookableServiceListener extends AbstractServiceListener{

	/**
	 * @see AbstractServiceListener::update()
	 */
	public function update( $service, ServiceContainerInterface $service_container ) {
		if ( $service instanceof Hookable ) {
			$service->hooks();
		}
	}
}
