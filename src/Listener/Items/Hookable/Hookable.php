<?php

namespace WPDesk\DiContainer\Listener\Items\Hookable;

/**
 * Interface Hookable, add hooks method to add wordpress actions and filters.
 * @package WPDesk\DiContainer\Listener\Hookable
 */
interface Hookable {

	/**
	 * @return void
	 */
	public function hooks();

}
